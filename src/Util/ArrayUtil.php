<?php

namespace GetRepo\Configurator\Util;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyAccess\PropertyPath;

class ArrayUtil
{
    private PropertyAccessorInterface $propertyAccessor;

    public function __construct()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex() // important for isReadable()
            ->enableExceptionOnInvalidPropertyPath() // important for isReadable()
            ->getPropertyAccessor();
    }

    public function arrayPush(array &$array, int $key, mixed $value): void
    {
        $array = array_merge(
            array_slice($array, 0, $key, true),
            [$value],
            array_slice($array, $key, null, true)
        );
    }

    public function unset(mixed &$array, PropertyPath $propertyPath): void
    {
        if ($this->propertyAccessor->isReadable($array, $propertyPath)) {
            $propertyPathParent = $propertyPath->getParent();
            if ($propertyPathParent) {
                $value = $this->propertyAccessor->getValue($array, $propertyPathParent);
                unset($value[$propertyPath->getElement($propertyPath->getLength() - 1)]);
                $this->propertyAccessor->setValue($array, $propertyPathParent, $value);
            } else {
                unset($array[$propertyPath->getElement(0)]);
            }
        }
    }
}
