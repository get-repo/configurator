<?php

namespace GetRepo\Configurator\Util;

use GetRepo\Configurator\Exception\ConfiguratorException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\String\CodePointString;

class DotEnvUtil
{
    private Filesystem $filesystem;

    public function __construct()
    {
        $this->filesystem = new Filesystem();
    }

    public function parse(string $path): array
    {
        if (!$this->filesystem->exists($path)) {
            throw new ConfiguratorException(sprintf('Env file "%s" was not found', $path));
        }

        $vars = [];
        $lines = array_filter(array_map('trim', explode(PHP_EOL, file_get_contents($path))));
        foreach ($lines as $i => $line) {
            $line = new CodePointString($line);
            $index = $line->indexOf('=');
            if (is_null($index)) {
                // check if its a comment
                if ($line->startsWith('#')) {
                    continue;
                }
                throw new ConfiguratorException(sprintf(
                    'Env file "%s" is invalid at line %d (%s)',
                    $path,
                    $i + 1,
                    $line,
                ));
            }

            $key = (string) $line->slice(0, $index);
            $value = $line->slice($index + 1);
            $v = $value->toString();
            $vars[$key] = match (true) {
                $value->isEmpty() => null,
                'null' === $v => null,
                is_numeric($v) => preg_match('/^\d+$/', $v) ? (int) $v : (float) $v,
                default => $v,
            };
        }

        return $vars;
    }

    public function populate(string $path, array $vars, bool $override = false): void
    {
        $existing = $this->parse($path);
        $content = [];
        foreach (array_merge($override ? [] : $existing, $vars) as $name => $value) {
            $content[] = sprintf('%s=%s', $name, match (true) {
                is_null($value) => 'null',
                '' === $value => '""',
                is_bool($value) => $value ? 1 : 0,
                default => $value,
            });
        }
        $this->filesystem->dumpFile($path, implode(PHP_EOL, $content) . PHP_EOL);
    }

    public function remove(string $path, string $var): bool
    {
        $existing = $this->parse($path);
        if ($exists = array_key_exists($var, $existing)) {
            unset($existing[$var]);
            $this->populate($path, $existing, true);
        }

        return $exists;
    }

    public function exists(string $name): bool
    {
        return array_key_exists($name, getenv());
    }

    public function getValue(string $name): string|false
    {
        return $this->exists($name) ? getenv($name) : false;
    }

    public function replaceExisting(string $value): string
    {
        $envs = getenv();
        $keys = array_map(function (string $key) {
            return sprintf('${%s}', $key);
        }, array_keys($envs));

        return str_replace($keys, array_values($envs), $value);
    }
}
