<?php

namespace GetRepo\Configurator\DependencyInjection;

use GetRepo\Configurator\Action\ActionInterface;
use GetRepo\Configurator\Configurator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class Container
{
    private ContainerBuilder $container;

    public function __construct()
    {
        $this->container = new ContainerBuilder();
        $loader = new YamlFileLoader($this->container, new FileLocator(realpath(__DIR__ . '/../../config')));
        $loader->load('services.yml');
        $this->container->set('configurator_container', $this);
        $this->container->compile();
    }

    public function getConfigurator(): Configurator
    {
        return $this->container->get(Configurator::class);
    }

    /**
     * @return ActionInterface[]
     */
    public function getActions(): array
    {
        $actions = [];
        $taggedActionClasses = array_keys($this->container->findTaggedServiceIds('getrepo_configurator.action'));
        foreach ($taggedActionClasses as $taggedActionClass) {
            $actions[call_user_func([$taggedActionClass, 'getName'])] = $this->container->get($taggedActionClass);
        }

        return $actions;
    }
}
