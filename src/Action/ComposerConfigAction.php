<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class ComposerConfigAction extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_COMPOSER];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('key')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('value')
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('options')
                    ->enumPrototype()
                        ->values([
                            '--global',
                            '--editor',
                            '--auth',
                            '--unset',
                            '--absolute',
                            '--json',
                            '--append',
                            '--quiet',
                            '--no-plugins',
                            '--no-scripts',
                            '--no-cache',
                            '-v',
                        ])
                    ->end()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return sprintf('%s = %s', $actionConfig['key'], $actionConfig['value']);
    }

    protected function doAction(array $actionConfig): void
    {
        $this->runComposer(array_merge(
            ['config'],
            $actionConfig['options'],
            [$actionConfig['key']],
            explode(' ', $actionConfig['value']),
        ));
    }
}
