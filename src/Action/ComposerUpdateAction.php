<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class ComposerUpdateAction extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_COMPOSER];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->arrayNode('options')
                    ->enumPrototype()
                        ->values([
                            '--prefer-source',
                            '--prefer-dist',
                            '--dev',
                            '--no-dev',
                            '--lock',
                            '--no-install',
                            '--no-audit',
                            '--no-autoloader',
                            '--no-suggest',
                            '--no-progress',
                            '--with-dependencies',
                            '--with-all-dependencies',
                            '--optimize-autoloader',
                            '--classmap-authoritative',
                            '--apcu-autoloader',
                            '--ignore-platform-reqs',
                            '--prefer-stable',
                            '--prefer-lowest',
                            '--interactive',
                            '--quiet',
                            '--ansi',
                            '--no-ansi',
                            '--no-plugins',
                            '--no-scripts',
                            '--no-cache',
                            '-v',
                        ])
                    ->end()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return '';
    }

    protected function doAction(array $actionConfig): void
    {
        $this->runComposer(array_merge(['update'], $actionConfig['options']));
    }
}
