<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class ComposerRunScriptAction extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_COMPOSER];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('name')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('options')
                    ->enumPrototype()
                        ->values([
                            '--dev',
                            '--no-dev',
                            '--quiet',
                            '--ansi',
                            '--no-ansi',
                            '--no-interaction',
                            '--no-plugins',
                            '--no-scripts',
                            '--no-cache',
                            '-v',
                        ])
                    ->end()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return '';
    }

    protected function doAction(array $actionConfig): void
    {
        $this->runComposer(array_merge(['run-script', $actionConfig['name']], $actionConfig['options']));
    }
}
