<?php

namespace GetRepo\Configurator\Action;

class CopyPathAction extends MovePathAction
{
    protected function doAction(array $actionConfig): void
    {
        $method = is_dir($actionConfig['path']) ? 'mirror' : 'copy';
        $this->filesystem->$method($actionConfig['path'], $actionConfig['to']);
    }
}
