<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class ComposerRequireAction extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_COMPOSER];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->arrayNode('dependencies')
                    ->beforeNormalization()
                        ->ifString()
                        ->then(function (string $dependency): array {
                            return [$dependency];
                        })
                    ->end()
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->scalarPrototype()
                        ->cannotBeEmpty()
                    ->end()
                ->end()
                ->arrayNode('options')
                    ->enumPrototype()
                        ->values([
                            '--dev',
                            '--prefer-source',
                            '--prefer-dist',
                            '--prefer-stable',
                            '--no-suggest',
                            '--no-progress',
                            '--no-update',
                            '--no-install',
                            '--no-audit',
                            '--update-no-dev',
                            '--update-with-dependencies',
                            '--update-with-all-dependencies',
                            '--with-dependencies',
                            '--with-all-dependencies',
                            '--sort-packages',
                            '--optimize-autoloader',
                            '--classmap-authoritative',
                            '--quiet',
                            '--no-plugins',
                            '--no-scripts',
                            '--no-cache',
                            '-v',
                        ])
                    ->end()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return implode(', ', $actionConfig['dependencies']);
    }

    protected function doAction(array $actionConfig): void
    {
        $this->runComposer(array_merge(['require'], $actionConfig['options'], $actionConfig['dependencies']));
    }
}
