<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class CreateDirectoryAction extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_FILE_SYSTEM];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('path')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->integerNode('mode')
                    ->defaultValue(0777)
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return $actionConfig['path'];
    }

    protected function doAction(array $actionConfig): void
    {
        $this->filesystem->mkdir($actionConfig['path'], $actionConfig['mode']);
    }
}
