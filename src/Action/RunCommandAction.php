<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class RunCommandAction extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_COMMAND];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('command')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return $actionConfig['command'];
    }

    protected function doAction(array $actionConfig): void
    {
        $process = Process::fromShellCommandline($actionConfig['command']);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }
}
