<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class RemoveEnv extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_ENV];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('name')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('path')
                    ->defaultNull()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return $actionConfig['name'];
    }

    protected function doAction(array $actionConfig): void
    {
        $path = $actionConfig['path'] ?? $this->settings['env']['env_file_path'];
        if (!$this->filesystem->exists($path)) {
            $this->filesystem->dumpFile($path, '');
        }
        $name = $actionConfig['name'];
        $this->dotEnvUtil->remove($path, $name);
        putenv($name); // remove env
    }
}
