<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class Env extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_ENV];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('name')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('value')
                    ->isRequired()
                ->end()
                ->scalarNode('path')
                    ->defaultNull()
                ->end()
                ->booleanNode('check_if_exists')
                    ->defaultFalse()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return $actionConfig['name'];
    }

    protected function doAction(array $actionConfig): void
    {
        $path = $actionConfig['path'] ?? $this->settings['env']['env_file_path'];
        if (!$this->filesystem->exists($path)) {
            $this->filesystem->dumpFile($path, '');
        }

        $name = $actionConfig['name'];
        if ($actionConfig['check_if_exists'] && $this->dotEnvUtil->exists($name)) {
            $value = $this->dotEnvUtil->getValue($name);
        } else {
            $value = $actionConfig['value'];
        }
        if (is_string($value)) {
            $value = $this->dotEnvUtil->replaceExisting($value);
        }
        $this->dotEnvUtil->populate($path, [$name => $value]);
        // load env
        putenv("$name=$value");
        $_ENV[$name] = $value;
    }
}
