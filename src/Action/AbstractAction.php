<?php

namespace GetRepo\Configurator\Action;

use GetRepo\Configurator\Exception\ConfiguratorException;
use GetRepo\Configurator\Util\ArrayUtil;
use GetRepo\Configurator\Util\DotEnvUtil;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\String\ByteString;
use Symfony\Component\Yaml\Yaml;

abstract class AbstractAction implements ActionInterface
{
    public const TAG_COMMAND = 'command';
    public const TAG_COMPOSER = 'composer';
    public const TAG_ENV = 'env';
    public const TAG_FILE_SYSTEM = 'filesystem';

    protected Processor $configProcessor;
    protected PropertyAccessorInterface $propertyAccessor;
    protected Filesystem $filesystem;
    protected array $settings;

    abstract protected function doAction(array $actionConfig): void;
    abstract protected function printMessage(array $actionConfig): string;

    public static function getName(): string
    {
        $matches = self::getByteString(static::class)->match('/\\\([^\\\]+)\\\(\w+)$/');

        return self::getByteString($matches[2])->replace('Action', '')->snake();
    }

    protected static function getByteString(string $string): ByteString
    {
        return new ByteString($string);
    }

    public function __construct(
        protected ArrayUtil $arrayUtil,
        protected DotEnvUtil $dotEnvUtil,
    ) {
        $this->configProcessor = new Processor();
        $this->propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->enableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor();
        $this->filesystem = new Filesystem();
    }

    public function __invoke(array $actionConfig, array $settings): void
    {
        $treeBuilder = new TreeBuilder(self::getName());
        /** @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();
        $rootNode
            ->children()
                ->arrayNode('tags')
                    ->scalarPrototype()->end()
                ->end()
                ->arrayNode('envs')
                    ->scalarPrototype()->end()
                ->end()
            ->end();
        $this->buildConfiguration($rootNode);
        $actionConfig = $this->configProcessor->process(
            configTree: $treeBuilder->buildTree(),
            configs: [$actionConfig],
        );
        array_walk_recursive($actionConfig, function (&$value) {
            if (is_string($value) && str_contains($value, '%root%')) {
                $value = str_replace('%root%', getcwd(), $value);
            }
        });

        $this->settings = $settings;
        // env filtering
        if ($actionConfig['envs'] && !in_array($settings['default_env'], $actionConfig['envs'])) {
            return;
        }
        $actionConfig['tags'] = $actionConfig['tags'] ?: static::getDefaultTags();
        // tags filtering
        if ($settings['tags'] && !array_intersect($settings['tags'], $actionConfig['tags'])) {
            return;
        }
        $this->write(self::getName(), 'yellow');
        $this->write(' ' . $this->printMessage($actionConfig), 'gray');
        $this->doAction($actionConfig);
        $this->write("\n");
    }

    protected function yamlParseFile(string $filename): array
    {
        return Yaml::parseFile(
            filename: $filename,
            flags: $this->settings['yaml']['flags_parse'],
        );
    }

    protected function yamlDumpFile(array $input, int $inline = null, int $indent = null, int $flags = null): string
    {
        return Yaml::dump(
            input: $input,
            inline: is_int($inline) ? $inline : $this->settings['yaml']['inline'],
            indent: is_int($indent) ? $indent : $this->settings['yaml']['indent'],
            flags: is_int($flags) ? $flags : $this->settings['yaml']['flags_dump'],
        );
    }

    protected function runComposer(array $command): Process
    {
        $composerPath = $this->settings['composer']['path'];
        if (!$composerPath) {
            throw new ConfiguratorException('Composer require action failed : Composer path is not set.');
        }
        // replace env vars in command
        array_walk($command, function (&$value) {
            $value = $this->dotEnvUtil->replaceExisting($value);
        });
        $process = new Process(array_merge(['php', $composerPath], $command));
        $process->setTimeout(null);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process;
    }

    protected function write(string $message, string $color = null): void
    {
        if ($this->settings['output'] instanceof OutputInterface) {
            if ($color) {
                $message = sprintf('<fg=%s>%s</>', $color, $message);
            }
            $this->settings['output']->write($message);
        }
    }
}
