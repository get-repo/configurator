<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class MovePathAction extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_FILE_SYSTEM];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('path')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('to')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return sprintf('%s > %s', $actionConfig['path'], $actionConfig['to']);
    }

    protected function doAction(array $actionConfig): void
    {
        $this->filesystem->rename($actionConfig['path'], $actionConfig['to']);
    }
}
