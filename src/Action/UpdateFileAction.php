<?php

namespace GetRepo\Configurator\Action;

use GetRepo\Configurator\Exception\ConfiguratorException;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Exception\InvalidDefinitionException;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class UpdateFileAction extends AbstractAction
{
    public const CHANGE_INSERT_LINE = 'insert-line';
    public const CHANGE_REPLACE_FIRST = 'replace-first';
    public const CHANGE_REPLACE_ALL = 'replace-all';
    public const CHANGE_PREG_REPLACE = 'preg-replace';

    private const MAPPING = [
        self::CHANGE_INSERT_LINE => ['required' => ['line', 'content']],
        self::CHANGE_REPLACE_FIRST => ['required' => ['search', 'content', 'exception_on_invalid_update']],
        self::CHANGE_REPLACE_ALL => ['required' => ['search', 'content', 'exception_on_invalid_update']],
        self::CHANGE_PREG_REPLACE => ['required' => ['search', 'content', 'exception_on_invalid_update']],
    ];

    public static function getDefaultTags(): array
    {
        return [self::TAG_FILE_SYSTEM];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('path')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('changes')
                    ->isRequired()
                    ->arrayPrototype()
                        ->validate()
                            ->always()
                            ->then(function (array $change): array {
                                $mapping = self::MAPPING[$change['change']] ?? false;
                                if (!$mapping) {
                                    throw new \RuntimeException(sprintf(
                                        'Mapping for "%s" does not exists',
                                        $change['change'],
                                    ));
                                }
                                $mapping['required'][] = 'change';
                                foreach ($mapping['required'] as $name) {
                                    if (!isset($change[$name])) {
                                        throw new InvalidDefinitionException(sprintf(
                                            'Change "%s" needs a "%s" value defined.',
                                            $change['change'],
                                            $name,
                                        ));
                                    }
                                }

                                return array_intersect_key($change, array_flip($mapping['required']));
                            })
                        ->end()
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->enumNode('change')
                                ->isRequired()
                                ->values(array_keys(self::MAPPING))
                            ->end()
                            ->scalarNode('content')->end()
                            ->integerNode('line')->end()
                            ->scalarNode('search')->end()
                            ->booleanNode('exception_on_invalid_update')
                                ->defaultFalse()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return $actionConfig['path'];
    }

    protected function doAction(array $actionConfig): void
    {
        $path = $actionConfig['path'];
        if (!$this->filesystem->exists($path)) {
            throw new FileNotFoundException(path: $path);
        }
        $content = file_get_contents($path);

        foreach ($actionConfig['changes'] as $change) {
            switch ($change['change']) {
                case self::CHANGE_INSERT_LINE:
                    $content = explode(PHP_EOL, $content); // string > array
                    $this->arrayUtil->arrayPush(
                        array: $content,
                        key: $change['line'],
                        value: $change['content'],
                    );
                    $content = implode(PHP_EOL, $content); // array > string
                    break;
                case self::CHANGE_REPLACE_FIRST:
                    $content = self::getByteString($content);
                    if ($change['exception_on_invalid_update'] && !$content->containsAny($change['search'])) {
                        throw new ConfiguratorException(sprintf(
                            'Update file action failed : Search term "%s" was not found in %s',
                            $change['search'],
                            $path,
                        ));
                    }
                    $parts = $content->split($change['search'], 2);
                    if (2 === count($parts)) {
                        $content = $parts[0] . $change['content'] . $parts[1];
                    }
                    break;
                case self::CHANGE_REPLACE_ALL:
                    $content = self::getByteString($content);
                    if ($change['exception_on_invalid_update'] && !$content->containsAny($change['search'])) {
                        throw new ConfiguratorException(sprintf(
                            'Update file action failed : Search term "%s" was not found in %s',
                            $change['search'],
                            $path,
                        ));
                    }
                    $content = $content->replace($change['search'], $change['content']);
                    break;
                case self::CHANGE_PREG_REPLACE:
                    $content = self::getByteString($content);
                    if ($change['exception_on_invalid_update'] && !$content->match($change['search'])) {
                        throw new ConfiguratorException(sprintf(
                            'Update file action failed : regexp term "%s" was not found in %s',
                            $change['search'],
                            $path,
                        ));
                    }
                    $content = $content->replaceMatches($change['search'], $change['content']);
                    break;
            }
        }

        $this->filesystem->dumpFile($path, $content);
    }
}
