<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class CreateYamlFileAction extends CreateFileAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_FILE_SYSTEM];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('path')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('content')
                    ->variablePrototype()->end()
                ->end()
                ->integerNode('inline')
                    ->defaultNull()
                ->end()
                ->integerNode('indent')
                    ->defaultNull()
                ->end()
                ->integerNode('flags')
                    ->defaultNull()
                ->end()
            ->end();
    }

    protected function doAction(array $actionConfig): void
    {
        $actionConfig['content'] = $this->yamlDumpFile(
            input: $actionConfig['content'],
            inline: $actionConfig['inline'],
            indent: $actionConfig['indent'],
            flags: $actionConfig['flags'],
        );
        parent::doAction($actionConfig);
    }
}
