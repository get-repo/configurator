<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\PropertyAccess\PropertyPath;

class UpdateYamlFileAction extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_FILE_SYSTEM];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('path')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('changes')
                    ->isRequired()
                    ->arrayPrototype()
                        ->validate()
                            ->ifTrue(function (array $change): bool {
                                return $change['remove'] && $change['content'];
                            })
                            ->thenInvalid('UpdateYamlFileAction "remove" and "content" keys are incompatible in %s')
                        ->end()
                        ->children()
                            ->scalarNode('property_path')
                                ->defaultNull()
                            ->end()
                            ->variableNode('content')
                                ->defaultNull()
                            ->end()
                            ->booleanNode('remove')
                                ->defaultFalse()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return $actionConfig['path'];
    }

    protected function doAction(array $actionConfig): void
    {
        $path = $actionConfig['path'];
        $yaml = $this->yamlParseFile($path);
        foreach ($actionConfig['changes'] as $change) {
            $propertyPath = $change['property_path'];
            $arrayAdd = false;
            if (str_ends_with($propertyPath, '[]')) {
                $propertyPath = substr($propertyPath, 0, -2);
                $arrayAdd = true;
            }
            $propertyPath = new PropertyPath($propertyPath);
            if ($change['remove']) {
                $this->arrayUtil->unset($yaml, $propertyPath);
            } else {
                if ($arrayAdd) {
                    $array = $this->propertyAccessor->getValue($yaml, $propertyPath);
                    if (!is_array($array)) {
                        throw new \InvalidArgumentException(sprintf(
                            'Property path "%s" can be applied on array, got %s',
                            $change['property_path'],
                            gettype($array),
                        ));
                    }
                    $array[] = $change['content'];
                    $change['content'] = $array;
                }

                $this->propertyAccessor->setValue($yaml, $propertyPath, $change['content']);
            }
        }

        $this->filesystem->dumpFile($path, $this->yamlDumpFile($yaml));
    }
}
