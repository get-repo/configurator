<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

interface ActionInterface
{
    public static function getName(): string;
    public static function getDefaultTags(): array;
    public function buildConfiguration(ArrayNodeDefinition $rootNode): void;
    public function __invoke(array $actionConfig, array $settings): void;
}
