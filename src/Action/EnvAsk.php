<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class EnvAsk extends Env
{
    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->arrayNode('names')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->scalarPrototype()
                        ->defaultNull()
                    ->end()
                ->end()
                ->scalarNode('path')
                    ->defaultNull()
                ->end()
                ->booleanNode('check_if_exists')
                    ->defaultFalse()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return "\n"; // will ask envs with output
    }

    protected function doAction(array $actionConfig): void
    {
        /** @var ?InputInterface $input */
        $input = $this->settings['input'];
        /** @var ?OutputInterface $output */
        $output = $this->settings['output'];
        foreach ($actionConfig['names'] as $name => $value) {
            if ($actionConfig['check_if_exists'] && $this->dotEnvUtil->exists($name)) {
                $value = $this->dotEnvUtil->getValue($name);
                if ($output) {
                    $output->writeln(sprintf('%s <fg=bright-green>(exists)</>: %s', $name, var_export($value, true)));
                }
            } elseif ($output) {
                $questionHelper = new QuestionHelper();
                $question = new Question(sprintf('%s <fg=gray>(%s)</>: ', $name, var_export($value, true)));
                $newValue = $questionHelper->ask($input, $output, $question);
                if (!is_null($newValue)) {
                    $value = $newValue;
                }
            }
            parent::doAction([
                'name' => $name,
                'value' => $value,
                'path' => $actionConfig['path'],
                'check_if_exists' => false, // check if exists above already
            ]);
        }
    }
}
