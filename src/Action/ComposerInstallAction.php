<?php

namespace GetRepo\Configurator\Action;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class ComposerInstallAction extends AbstractAction
{
    public static function getDefaultTags(): array
    {
        return [self::TAG_COMPOSER];
    }

    public function buildConfiguration(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->arrayNode('options')
                    ->enumPrototype()
                        ->values([
                            '--prefer-source',
                            '--prefer-dist',
                            '--dev',
                            '--no-dev',
                            '--dry-run',
                            '--download-only',
                            '--no-suggest',
                            '--no-autoloader',
                            '--no-progress',
                            '--no-install',
                            '--audit',
                            '--optimize-autoloader',
                            '--classmap-authoritative',
                            '--apcu-autoloader',
                            '--ignore-platform-reqs',
                            '--quiet',
                            '--ansi',
                            '--no-ansi',
                            '--no-interaction',
                            '--no-plugins',
                            '--no-scripts',
                            '--no-cache',
                            '-v',
                        ])
                    ->end()
                ->end()
            ->end();
    }

    protected function printMessage(array $actionConfig): string
    {
        return '';
    }

    protected function doAction(array $actionConfig): void
    {
        $this->runComposer(array_merge(['install'], $actionConfig['options']));
    }
}
