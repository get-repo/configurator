<?php

namespace GetRepo\Configurator;

use GetRepo\Configurator\Configuration\ConfiguratorConfiguration;
use GetRepo\Configurator\DependencyInjection\Container;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class Configurator
{
    private Processor $configProcessor;

    public function __construct(
        #[Autowire(service: 'configurator_container')]
        private Container $container,
        private ConfiguratorConfiguration $configuratorConfiguration,
    ) {
        $this->configProcessor = new Processor();
    }

    public function __invoke(
        array $configuration,
        array $tags = [],
        string $env = null,
        InputInterface $input = null,
        OutputInterface $output = null,
    ): void {
        $configuration = $this->configProcessor->processConfiguration(
            configuration: $this->configuratorConfiguration,
            configs: [$configuration],
        );
        $configuration['settings']['tags'] = $tags; // tags filtering is in AbstractAction
        if ($env) {
            $configuration['settings']['default_env'] = $env; // env filtering is in AbstractAction
        }
        $configuration['settings']['input'] = $input;
        $configuration['settings']['output'] = $output;
        $actions = $this->container->getActions();
        foreach ($configuration['actions'] as $config) {
            $action = $actions[$config['action']];
            unset($config['action']);
            $action(actionConfig: $config, settings: $configuration['settings']);
        }
    }
}
