<?php

namespace GetRepo\Configurator\Configuration;

use GetRepo\Configurator\DependencyInjection\Container;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Process\Process;

final class ConfiguratorConfiguration implements ConfigurationInterface
{
    private array $actions;

    public function __construct(
        #[Autowire(service: 'configurator_container')]
        Container $container
    ) {
        $this->actions = array_keys($container->getActions());
    }

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('configurator');
        /** @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        // get composer path
        $process = new Process(['which', 'composer']);
        $process->run();
        $composerPath = $process->isSuccessful() ? trim($process->getOutput()) : null;

        $rootNode
            ->children()
                ->arrayNode('settings')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('default_env')
                            ->defaultNull()
                        ->end()
                        ->arrayNode('env')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('env_file_path')
                                    ->defaultValue('.env')
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('composer')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('path')
                                    ->defaultValue($composerPath)
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('yaml')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->integerNode('inline')
                                    ->defaultValue(2)
                                ->end()
                                ->integerNode('indent')
                                    ->defaultValue(4)
                                ->end()
                                ->integerNode('flags_dump')
                                    ->defaultValue(0)
                                ->end()
                                ->integerNode('flags_parse')
                                    ->defaultValue(0)
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('actions')
                    ->arrayPrototype()
                        ->validate()
                            ->ifTrue(function ($action): bool {
                                return empty($action['action']);
                            })
                            ->thenInvalid('"action" is required')
                        ->end()
                        ->validate()
                            ->ifTrue(function ($action): bool {
                                return !in_array($action['action'], $this->actions);
                            })
                            ->thenInvalid(sprintf(
                                'action "%%s" is invalid. Available actions are "%s"',
                                implode('", "', $this->actions),
                            ))
                        ->end()
                        ->variablePrototype()->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
