<?php

namespace Tests;

use GetRepo\Configurator\Configurator;
use GetRepo\Configurator\DependencyInjection\Container;
use GetRepo\Configurator\Exception\ConfiguratorException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\String\ByteString;
use Symfony\Component\Yaml\Yaml;

/*
 * phpcs:ignoreFile Generic.Files.LineLength.TooLong
 */
class ConfiguratorTest extends TestCase
{
    private static Configurator $configurator;

    public static function setUpBeforeClass(): void
    {
        $container = new Container();
        self::$configurator = $container->getConfigurator();
    }

    public function tearDown(): void
    {
        $testPath = self::getTestPath();
        // clear test path after each test
        (new Filesystem())->remove($testPath);
    }

    public static function actionsProvider(): array
    {
        $testPath = self::getTestPath();

        return [
            'empty actions' => [[]], // just checking no errors
            // CREATE
            'create file: empty file content' => [
                [['action' => 'create_file', 'path' => ($file = "{$testPath}/test_create")]],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, ''],
                ],
            ],
            'create file: file content' => [
                [$createFileAction = ['action' => 'create_file', 'path' => $file, 'content' => $content = 'OK']],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, $content],
                ],
            ],
            'create file: file content with new lines' => [
                [['action' => 'create_file', 'path' => $file, 'content' => $content = "new\nLines"]],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, $content],
                ],
            ],
            'create yaml file: yaml content' => [
                [$createYamlFileAction = ['action' => 'create_yaml_file', 'path' => $file, 'content' => ['test' => 1]]],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "test: 1\n"],
                ],
            ],
            'create yaml file: yaml content with inline' => [
                [$createYamlFileAction],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "{ test: 1 }"],
                ],
                ['yaml' => ['inline' => 0]],
            ],
            'create yaml file: yaml content with indent' => [
                [array_merge($createYamlFileAction, ['content' => ['test' => [1]]])],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, sprintf("test:\n%s- 1\n", str_repeat(' ', 32))],
                ],
                ['yaml' => ['indent' => 32]],
            ],
            'create yaml file: yaml content with flag date handling' => [
                [array_merge($createYamlFileAction, ['content' => ['2016-05-27']])],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "- '2016-05-27'\n"],
                ],
                ['yaml' => ['flags_parse' =>  Yaml::PARSE_DATETIME]],
            ],
            'create yaml file: yaml content with specific indent' => [
                [array_merge($createYamlFileAction, ['content' => ['test_indent' => [1]], 'indent' => 7])],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "test_indent:\n       - 1\n"],
                ],
            ],
            'create yaml file: yaml content with specific inline' => [
                [array_merge($createYamlFileAction, ['content' => ['test_inline' => [[1]]], 'inline' => 0])],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, '{ test_inline: [[1]] }'],
                ],
            ],
            'create yaml file: yaml content with specific flags' => [
                [array_merge($createYamlFileAction, ['content' => ['a' => null], 'flags' => Yaml::DUMP_NULL_AS_TILDE])],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "a: ~\n"],
                ],
            ],
            'create directory: new dir default permissions' => [
                [$createDirAction = ['action' => 'create_directory', 'path' => ($dir = "{$testPath}/new_directory")]],
                [
                    'directory_exists' => [$dir],
                    'directory_is_readable' => [$dir],
                ],
            ],
            'create directory: new dir 0700 permissions' => [
                [['action' => 'create_directory', 'path' => $dir, 'mode' => 0700]],
                [
                    'directory_exists' => [$dir],
                ],
            ],
            // UPDATE
            'update file: insert new line at the end' => [
                [$createFileAction, ['action' => 'update_file', 'path' => $file, 'changes' => [
                    ['change' => 'insert-line', 'line' => 6, 'content' => 'new line'],
                ]]],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "OK\nnew line"],
                ],
            ],
            'update file: insert new line at first position' => [
                [$createFileAction, ['action' => 'update_file', 'path' => $file, 'changes' => [
                    ['change' => 'insert-line', 'line' => 0, 'content' => 'new line'],
                ]]],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "new line\nOK"],
                ],
            ],
            'update file: replace first multiple times' => [
                [
                    $createFileToUpdateLater = [
                        'action' => 'create_file',
                        'path' => ($file = "{$testPath}/test_update"),
                        'content' => "LINE 1(FIRST)\nLINE 2\nLINE 3\nLINE 4 (LAST)",
                    ],
                    [
                        'action' => 'update_file',
                        'path' => $file,
                        'changes' => [
                            ['change' => 'replace-first', 'search' => 'LINE', 'content' => 'REPLACED'],
                            ['change' => 'replace-first', 'search' => 'LAST', 'content' => 'REPLACED'],
                            ['change' => 'replace-first', 'search' => "\n", 'content' => '**CUT**'],
                            ['change' => 'replace-first', 'search' => ' (REPLACED)', 'content' => ''],
                        ],
                    ],
                ],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "REPLACED 1(FIRST)**CUT**LINE 2\nLINE 3\nLINE 4"],
                ],
            ],
            'update file: replace all multiple times' => [
                [$createFileToUpdateLater, ['action' => 'update_file', 'path' => $file, 'changes' => [
                    ['change' => 'replace-all', 'search' => 'LINE', 'content' => 'REPLACED'],
                    ['change' => 'replace-all', 'search' => 'NOTHING', 'content' => 'FAIL'],
                    ['change' => 'replace-all', 'search' => 'REPLACED', 'content' => 'DONE'],
                ]]],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "DONE 1(FIRST)\nDONE 2\nDONE 3\nDONE 4 (LAST)"],
                ],
            ],
            'update yaml file: add and replace items multiple times' => [
                [
                    array_merge($createYamlFileAction, ['path' => $file = "{$testPath}/update_yaml_test.yaml"]),
                    [
                        'action' => 'update_yaml_file',
                        'path' => $file,
                        'changes' => [
                            ['property_path' => '[new]', 'content' => ['NEW CONTENT']],
                            ['property_path' => '[new]', 'content' => true],
                            ['property_path' => '[test]', 'content' => 2],
                        ],
                    ],
                ],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "test: 2\nnew: true\n"],
                ],
            ],
            'update file: replace first invalid with exception' => [
                [
                    $createFileToUpdateLater = [
                        'action' => 'create_file',
                        'path' => ($file = "{$testPath}/test_update"),
                        'content' => "just this",
                    ],
                    [
                        'action' => 'update_file',
                        'path' => $file,
                        'changes' => [
                            [
                                'change' => 'replace-first',
                                'search' => 'INVALID_STRING_NOT_FOUND',
                                'content' => 'A',
                                'exception_on_invalid_update' => true,
                            ],
                        ],
                    ],
                ],
                [
                    'exception' => [ConfiguratorException::class],
                    'exceptionMessage' => ['Update file action failed : Search term "INVALID_STRING_NOT_FOUND" was not found in /tmp/configurator/test_update'],
                ],
            ],
            'update file: replace all invalid with exception' => [
                [
                    $createFileToUpdateLater = [
                        'action' => 'create_file',
                        'path' => ($file = "{$testPath}/test_update"),
                        'content' => "just this",
                    ],
                    [
                        'action' => 'update_file',
                        'path' => $file,
                        'changes' => [
                            [
                                'change' => 'replace-all',
                                'search' => 'INVALID_STRING_NOT_FOUND',
                                'content' => 'A',
                                'exception_on_invalid_update' => true,
                            ],
                        ],
                    ],
                ],
                [
                    'exception' => [ConfiguratorException::class],
                    'exceptionMessage' => ['Update file action failed : Search term "INVALID_STRING_NOT_FOUND" was not found in /tmp/configurator/test_update'],
                ],
            ],
            'update file: preg replace' => [
                [$createFileToUpdateLater,
                    [
                        'action' => 'update_file',
                        'path' => $file,
                        'changes' => [
                            [
                                'change' => 'preg-replace',
                                'search' => '/(\s+)([^$]+)$/',
                                'content' => '$1REPLACED',
                            ],
                        ],
                    ],
                ],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, 'just REPLACED'],
                ],
            ],
            'update file: preg replace invalid with exception' => [
                [$createFileToUpdateLater,
                    [
                        'action' => 'update_file',
                        'path' => $file,
                        'changes' => [
                            [
                                'change' => 'preg-replace',
                                'search' => '/does not match anything/',
                                'content' => '',
                                'exception_on_invalid_update' => true,
                            ],
                        ],
                    ],
                ],
                [
                    'exception' => [ConfiguratorException::class],
                    'exceptionMessage' => ['Update file action failed : regexp term "/does not match anything/" was not found in /tmp/configurator/test_update'],
                ],
            ],
            'update yaml file: using [] property path notation' => [
                [
                    [
                        'action' => 'create_yaml_file',
                        'path' => $file,
                        'content' => ['test' => [1, 2, 3]],
                    ],
                    [
                        'action' => 'update_yaml_file',
                        'path' => $file,
                        'changes' => [
                            ['property_path' => '[test][]', 'content' => 4],
                        ],
                    ],
                ],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "test:\n    - 1\n    - 2\n    - 3\n    - 4\n"],
                ],
            ],
            'update yaml file: remove property path' => [
                [
                    array_merge($createYamlFileAction, ['path' => $file = "{$testPath}/update_yaml_test.yaml"]),
                    [
                        'action' => 'update_yaml_file',
                        'path' => $file,
                        'changes' => [
                            ['property_path' => '[new]', 'content' => 'SHOULD BE REMOVED'],
                            ['property_path' => '[new]', 'remove' => true],
                        ],
                    ],
                ],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "test: 1\n"],
                ],
            ],
            'update yaml file: remove property path deep' => [
                [
                    array_merge($createYamlFileAction, ['path' => $file = "{$testPath}/update_yaml_test.yaml"]),
                    [
                        'action' => 'update_yaml_file',
                        'path' => $file,
                        'changes' => [
                            ['property_path' => '[new][deep][test_1]', 'content' => 1],
                            ['property_path' => '[new][deep][test_2]', 'content' => 2],
                            ['property_path' => '[new][deep][test_2]', 'remove' => true],
                        ],
                    ],
                ],
                [
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, "test: 1\nnew:\n    deep: { test_1: 1 }\n"],
                ],
            ],
            'move file' => [
                [
                    ['action' => 'create_file', 'path' => ($fileToMove = "{$testPath}/move_me")],
                    ['action' => 'move_path', 'path' => $fileToMove, 'to' => $movedFile = "{$testPath}/moved_file"],
                ],
                [
                    'file_does_not_exist' => [$fileToMove],
                    'file_exists' => [$movedFile],
                ],
            ],
            'copy file' => [
                [
                    ['action' => 'create_file', 'path' => ($fileToCopy = "{$testPath}/copy_me"), 'content' => 'ZZZZZ'],
                    ['action' => 'copy_path', 'path' => $fileToCopy, 'to' => $copiedFile = "{$testPath}/copied_file"],
                ],
                [
                    'file_exists' => [$copiedFile],
                    'string_equals_file' => [$copiedFile, 'ZZZZZ'],
                ],
            ],
            'copy directory' => [
                [
                    ['action' => 'create_directory', 'path' => $dir = "{$testPath}/scenario2"],
                    ['action' => 'create_file', 'path' => ($fileToCopy = "{$dir}/copy_me"), 'content' => 'COPY_DIR'],
                    ['action' => 'copy_path', 'path' => $dir, 'to' => $copiedDir = "{$testPath}/copied_dir"],
                ],
                [
                    'directory_exists' => [$copiedDir],
                    'file_exists' => [$copiedFile = "{$copiedDir}/copy_me"],
                    'string_equals_file' => [$copiedFile, 'COPY_DIR'],
                ],
            ],
            // SYMLINK
            'symlink dirs' => [
                [$createDirAction, [
                    'action' => 'create_symlink',
                    'path' => "{$testPath}/new_directory",
                    'target' => $symlinkDir = "{$testPath}/my_symlink",
                ]],
                [
                    'directory_exists' => [$symlinkDir],
                ],
            ],
            // REMOVE
            'remove not existing file' => [
                [['action' => 'remove_path', 'path' => "{$file}does_not_exists"]],
                [
                    'file_does_not_exist' => [$file],
                ],
            ],
            'remove path: new file and remove' => [
                [$createFileAction, ['action' => 'remove_path', 'path' => $file]],
                [
                    'file_does_not_exist' => [$file],
                ],
            ],
            'remove not existing dir' => [
                [['action' => 'remove_path', 'path' => "{$dir}/does_not_exists"]],
                [
                    'file_does_not_exist' => [$dir],
                ],
            ],
            'remove path: new dir and remove' => [
                [$createDirAction, ['action' => 'remove_path', 'path' => $dir]],
                [
                    'directory_does_not_exist' => [$dir],
                ],
            ],
            // COMPOSER
            /** @see composer_test.php */
            'composer require' => [
                [['action' => 'composer_require', 'dependencies' => ['a/b', 'c/d'], 'options' => ['--no-scripts']]],
                [
                    'file_exists' => ["{$testPath}/composer"],
                    'string_equals_file' => ["{$testPath}/composer", 'require --no-scripts a/b c/d'],
                ],
                $composerSettings = ['composer' => ['path' => __DIR__ . '/composer_test.php']],
            ],
            /** @see composer_test.php */
            'composer require with env var' => [
                [['action' => 'composer_require', 'dependencies' => ['a/b:${KERNEL_CLASS}']]],
                [
                    'file_exists' => ["{$testPath}/composer"],
                    'string_equals_file' => ["{$testPath}/composer", 'require a/b:Test\Kernel'],
                ],
                $composerSettings,
            ],
            /** @see composer_test.php */
            'composer install' => [
                [['action' => 'composer_install', 'options' => ['--no-dev', '--optimize-autoloader']]],
                [
                    'file_exists' => ["{$testPath}/composer"],
                    'string_equals_file' => ["{$testPath}/composer", 'install --no-dev --optimize-autoloader'],
                ],
                $composerSettings,
            ],
            /** @see composer_test.php */
            'composer update' => [
                [['action' => 'composer_update', 'options' => ['--dev']]],
                [
                    'file_exists' => ["{$testPath}/composer"],
                    'string_equals_file' => ["{$testPath}/composer", 'update --dev'],
                ],
                $composerSettings,
            ],
            /** @see composer_test.php */
            'composer run script' => [
                [['action' => 'composer_run_script', 'name' => 'test', 'options' => ['--no-dev']]],
                [
                    'file_exists' => ["{$testPath}/composer"],
                    'string_equals_file' => ["{$testPath}/composer", 'run-script test --no-dev'],
                ],
                $composerSettings,
            ],
            /** @see composer_test.php */
            'composer config (with composer tag)' => [
                [[
                    'action' => 'composer_config',
                    'key' => 'repositories.aaaa/bbbb',
                    'value' => 'git git@gitlab.com:bbbb/bbbb.git',
                    'options' => ['--global'],
                ]],
                [
                    'file_exists' => ["{$testPath}/composer"],
                    'string_equals_file' => [
                        "{$testPath}/composer",
                        'config --global repositories.aaaa/bbbb git git@gitlab.com:bbbb/bbbb.git',
                    ],
                ],
                $composerSettings,
                ["composer"], // custom tags
            ],
            // TAGS
            'default tag "filesystem"' => [
                [$TagTestAction = ['action' => 'create_file', 'path' => $file = "{$testPath}/file1"]],
                ['file_exists' => [$file]],
                [], // no settings
                ["filesystem"], // custom tags
            ],
            'no custom tag but "another_tag" is called' => [
                [$TagTestAction],
                ['file_does_not_exist' => [$file]],
                [], // no settings
                ["another_tag"], // custom tags
            ],
            'custom tag "test" is set but "another_tag" is called' => [
                [array_merge($TagTestAction, ['tags' => ['test']])],
                ['file_does_not_exist' => [$file]],
                [], // no settings
                ["another_tag"], // custom tags
            ],
            'custom tag "test" is set and "test" is called' => [
                [array_merge($TagTestAction, ['tags' => ['test']])],
                ['file_exists' => [$file]],
                [], // no settings
                ["test", "another_tag"], // custom tags
            ],
            // ENV FILTER
            'action env filtered and not executed' => [
                $action = [['action' => 'create_file', 'path' => ($file = "{$testPath}/nothing"), 'envs' => ['test']]],
                ['file_does_not_exist' => [$file]],
            ],
            'action env filtered with env defined and not executed' => [
                $action,
                ['file_does_not_exist' => [$file]],
                [], // no settings
                [], // custom tags
                'whatever', // env
            ],
            'action env filtered and executed' => [
                $action,
                ['file_exists' => [$file]],
                [], // no settings
                [], // custom tags
                'test', // env
            ],
            // ENV
            'create env var' => [
                [
                    ['action' => 'env', 'name' => 'MY_TEST', 'value' => 999],
                    ['action' => 'env', 'name' => 'ANOTHER_KEY', 'value' => 'value'],
                ],
                [
                    'file_exists' => [$envFile = "{$testPath}/.env"],
                    'string_equals_file' => [$envFile, "MY_TEST=999\nANOTHER_KEY=value\n"],
                ],
                $envFileSettings = ['env' => ['env_file_path' => $envFile]],
            ],
            'create env var replace existing in value' => [
                [
                    ['action' => 'env', 'name' => 'TEST', 'value' => 'replaced ${KERNEL_CLASS}'],
                ],
                [
                    'file_exists' => [$envFile],
                    'string_equals_file' => [$envFile, "TEST=replaced Test\Kernel\n"],
                ],
                $envFileSettings
            ],
            'create env var with check if exists' => [
                [
                    ['action' => 'env', 'name' => 'KERNEL_CLASS', 'value' => 0, 'check_if_exists' => true],
                ],
                [
                    'file_exists' => [$envFile],
                    'string_equals_file' => [$envFile, "KERNEL_CLASS=Test\Kernel\n"],
                ],
                $envFileSettings
            ],
            'create env var with override path' => [
                [
                    ['action' => 'env', 'name' => 'Z', 'value' => 9, 'path' => $EnvLocal = "{$testPath}/.env.local"],
                ],
                [
                    'file_exists' => [$EnvLocal],
                    'string_equals_file' => [$EnvLocal, "Z=9\n"],
                ],
            ],
            'ask env vars' => [
                [
                    ['action' => 'env_ask', 'names' => ['A' => '1', 'B' => '2']],
                ],
                [
                    'file_exists' => [$envFile],
                    'string_equals_file' => [$envFile, "A=1\nB=2\n"],
                ],
                $envFileSettings,
            ],
            'ask env vars with check if exists' => [
                [
                    ['action' => 'env_ask', 'names' => ['KERNEL_CLASS' => 'NEW VALUE'], 'check_if_exists' => true],
                ],
                [
                    'file_exists' => [$envFile],
                    'string_equals_file' => [$envFile, "KERNEL_CLASS=Test\Kernel\n"],
                ],
                $envFileSettings
            ],
            'ask env vars with override path' => [
                [
                    ['action' => 'env_ask', 'names' => ['Y' => 'OK'], 'path' => $envFile = "{$testPath}/.env.ask"],
                ],
                [
                    'file_exists' => [$envFile],
                    'string_equals_file' => [$envFile, "Y=OK\n"],
                ],
            ],
            'remove env vars' => [
                [
                    [
                        'action' => 'create_file',
                        'path' => $envFile = "{$testPath}/.env.remove",
                        'content' => "AAAA=11\nBBBB=22\nCCCC=33",
                    ],
                    ['action' => 'remove_env', 'name' => 'BBBB', 'path' => $envFile],
                ],
                [
                    'file_exists' => [$envFile],
                    'string_equals_file' => [$envFile, "AAAA=11\nCCCC=33\n"],
                ],
            ],
            // SCENARIOS
            'scenario files' => [
                [
                    ['action' => 'create_directory', 'path' => $dir = "{$testPath}/scenario1"],
                    ['action' => 'create_file', 'path' => $file = "{$testPath}/scenario1/file.txt", 'content' => 'AAA'],
                    [
                        'action' => 'update_file',
                        'path' => $file,
                        'changes' => [
                            [
                                'change' => 'replace-all',
                                'search' => 'AAA',
                                'content' => 'This is a scenario test',
                            ],
                            [
                                'change' => 'replace-first',
                                'search' => 'test',
                                'content' => 'with many tests',
                            ],
                        ],
                    ],
                ],
                [
                    'directory_exists' => [$dir],
                    'file_exists' => [$file],
                    'string_equals_file' => [$file, 'This is a scenario with many tests'],
                ],
            ],
        ];
    }

    /**
     * @dataProvider actionsProvider
     */
    public function testActions(
        array $actions,
        array $asserts = [],
        array $settings = null,
        array $tags = [],
        string $env = null,
    ): void {
        $this->doAssert($asserts, 'expect');
        $configuration = ['actions' => $actions];
        if ($settings) {
            $configuration['settings'] = $settings;
        }
        self::$configurator->__invoke(configuration: $configuration, tags: $tags, env: $env);
        $this->doAssert($asserts, 'assert');
        $this->assertTrue(true); // makes PHPUnit happy if no asserts :)
    }

    private function doAssert(array $asserts, string $prefix): void
    {
        foreach ($asserts as $assert => $args) {
            $assert = (new ByteString($assert))->camel()->title();
            $guesses = [sprintf('assert%s', $assert), sprintf('expect%s', $assert)];
            foreach ($guesses as $guess) {
                $callback = [$this, $guess];
                if (method_exists($callback[0], $callback[1]) && str_starts_with($callback[1], $prefix)) {
                    call_user_func_array($callback, (array) $args);
                }
            }
        }
    }

    private static function getTestPath(): string
    {
        $filesystem = new Filesystem();
        $testPath = sys_get_temp_dir() . '/configurator';
        if (!$filesystem->exists($testPath)) {
            $filesystem->mkdir($testPath);
        }

        return $testPath;
    }
}
