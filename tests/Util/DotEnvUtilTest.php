<?php

namespace Tests\Util;

use GetRepo\Configurator\Util\DotEnvUtil;
use PHPUnit\Framework\TestCase;

class DotEnvUtilTest extends TestCase
{
    private DotEnvUtil $dotEnvUtil;

    public function setUp(): void
    {
        $this->dotEnvUtil = new DotEnvUtil();
    }

    public static function parseProvider(): array
    {
        return [
            // exceptions
            'invalid var' => [[], 'INVALID', 'invalid at line 1 (INVALID)'],
            'invalid var with comments' => [[], "# comment\nINVALID", 'invalid at line 2 (INVALID)'],
            // success
            'empty file' => [[], ''],
            'only empty lines' => [[], "\n   \n      \n       \n"],
            'multiple invalid var' => [[], "INVALID\nINVALID2\n", 'invalid at line 1 (INVALID)'],
            'values with one empty' => [
                ['AAAA' => 1, 'BBBB' => 'this is a test', 'CCCC' => null],
                "AAAA=1\nBBBB=this is a test\nCCCC=\n",
            ],
            'numeric and float values' => [
                ['INT' => 10, 'FLOAT' => 5.89],
                "INT=10\nFLOAT=5.89",
            ],
            'values with equals in it' => [
                ['TEST' => 'there is an = in the value'],
                "TEST=there is an = in the value",
            ],
            'null values' => [
                ['NULL' => null],
                "NULL=null",
            ],
        ];
    }

    /**
     * @dataProvider parseProvider
     */
    public function testParse(array $expected, string $content, string $exception = null): void
    {
        $path = sys_get_temp_dir() . '/.configurator.test.env';
        if (file_exists($path)) {
            unlink($path);
        }
        file_put_contents($path, $content);
        if ($exception) {
            $this->expectExceptionMessage($exception);
        }
        $this->assertSame($expected, $this->dotEnvUtil->parse($path));
    }

    public static function populateProvider(): array
    {
        return [
            'empty file and empty vars' => [[], '', []],
            'empty file and some vars' => [['A' => 1], '', ['A' => 1]],
            'null vars' => [['NULL' => null], '', ['NULL' => null]],
            'file and empty vars' => [['TEST' => 1, 'A' => 1], 'TEST=1', ['A' => 1]],
            'existing vars' => [['B' => 1], 'B=existing', ['B' => 1]],
        ];
    }

    /**
     * @dataProvider populateProvider
     */
    public function testPopulate(array $expected, string $content, array $vars): void
    {
        $path = sys_get_temp_dir() . '/.configurator.test.env';
        if (file_exists($path)) {
            unlink($path);
        }
        file_put_contents($path, $content);
        $this->dotEnvUtil->populate($path, $vars);
        $this->assertSame($expected, $this->dotEnvUtil->parse($path));
    }

    public static function removeProvider(): array
    {
        return [
            'empty file and empty vars' => [false, '', ''],
            'empty file and some vars' => [false, "AAAA=1\nBBBBBB=222", 'CCCCC'],
            'remove var' => [true, "AAAA=1\nBBBBBB=222", 'AAAA'],
        ];
    }

    /**
     * @dataProvider removeProvider
     */
    public function testRemove(bool $expected, string $content, string $var): void
    {
        $path = sys_get_temp_dir() . '/.configurator.test.env';
        if (file_exists($path)) {
            unlink($path);
        }
        file_put_contents($path, $content);
        $this->assertEquals($expected, $this->dotEnvUtil->remove($path, $var));
        $this->assertEquals(false, $this->dotEnvUtil->exists($var));
    }

    public function testExists(): void
    {
        $this->assertFalse($this->dotEnvUtil->exists('ZZZZZZZ'));
        $this->assertTrue($this->dotEnvUtil->exists('KERNEL_CLASS'));
    }

    public function testGetValue(): void
    {
        $this->assertFalse($this->dotEnvUtil->getValue('ZZZZZZZ'));
        $this->assertEquals('Test\Kernel', $this->dotEnvUtil->getValue('KERNEL_CLASS'));
    }

    public function testReplaceExisting(): void
    {
        $this->assertEquals('--${ZZZZZZZ}--', $this->dotEnvUtil->replaceExisting('--${ZZZZZZZ}--'));
        $this->assertEquals('--Test\Kernel--', $this->dotEnvUtil->replaceExisting('--${KERNEL_CLASS}--'));
    }
}
