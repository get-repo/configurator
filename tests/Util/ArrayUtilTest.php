<?php

namespace Tests\Util;

use GetRepo\Configurator\Util\ArrayUtil;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyAccess\PropertyPath;

class ArrayUtilTest extends TestCase
{
    private const TEST_ARRAY = ['first', 'seconds', 'third'];
    private const TEST_ARRAY_UNSET = ['aaaa' => ['bbbb' => ['cccc' => 1, 'dddd' => 2]]];

    private ArrayUtil $arrayUtil;

    public function setUp(): void
    {
        $this->arrayUtil = new ArrayUtil();
    }

    public static function arrayPushProvider(): array
    {
        return [
            'add key 0 value NULL' => [array_merge([null], self::TEST_ARRAY), 0, null],
            'add key 0 value FALSE' => [array_merge([false], self::TEST_ARRAY), 0, false],
            'add key 0 value TRUE' => [array_merge([true], self::TEST_ARRAY), 0, true],
            'add key 1 value test string' => [['first', 'test', 'seconds', 'third'], 1, 'test'],
            'add key 999 value array' => [['first', 'seconds', 'third', ['test']], 999, ['test']],
        ];
    }

    /**
     * @dataProvider arrayPushProvider
     */
    public function testArrayPush(array $expected, int $key, mixed $value): void
    {
        $array = self::TEST_ARRAY;
        $this->arrayUtil->arrayPush(array: $array, key: $key, value: $value);
        $this->assertEquals($expected, $array);
    }

    public static function unsetProvider(): array
    {
        return [
            'remove un-existing' => [self::TEST_ARRAY_UNSET, '[existing]'],
            'remove un-existing deep' => [self::TEST_ARRAY_UNSET, '[aaaa][existing]'],
            'remove cccc' => [['aaaa' => ['bbbb' => ['dddd' => 2]]], '[aaaa][bbbb][cccc]'],
            'remove bbbb' => [['aaaa' => []], '[aaaa][bbbb]'],
            'remove aaaa' => [[], '[aaaa]'],
        ];
    }

    /**
     * @dataProvider unsetProvider
     */
    public function testUnset(array $expected, string $propertyPath): void
    {
        $array = self::TEST_ARRAY_UNSET;
        $this->arrayUtil->unset(array: $array, propertyPath: new PropertyPath($propertyPath));
        $this->assertEquals($expected, $array);
    }
}
