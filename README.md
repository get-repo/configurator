<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/52355537/configurator_logo.png" height=100 />
</p>

<h1 align=center>Configurator</h1>

<br/>

Configurator is a simple tool designed to setup and configure process for any project.<br/>
It automates file creation, updates, and pivotal actions defined within a YAML configuration file.

## Table of Contents

- [Installation](#installation)
- [How To Use It](#how-to-use-it)
- [Configuration Reference](#configuration-reference)
  - [Environment Variable Actions](#environment-variable-actions)
  - [File System Actions](#file-system-actions)
  - [Composer Actions](#composer-actions)
  - [Command Action](#command-action)


<br/><br/>


## Installation

### Installation with composer

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/configurator git https://gitlab.com/get-repo/configurator.git
    composer require get-repo/configurator


<br/><br/>


## How To Use It


Configurator can be used as a service within your project's codebase.<br/>
Alternatively, Configurator provides a user-friendly Command-Line Interface (CLI) accessible via bin/configurator.<br/>
It offers a straightforward way to execute Configurator's functionalities by simply invoking commands from the terminal.

Whether you prefer integrating Configurator's functionalities directly within your project or utilizing its standalone CLI for swift configurations, Configurator caters to diverse developer preferences, offering a seamless experience tailored to individual workflow preferences."

### Service

```php
use GetRepo\Configurator\DependencyInjection\Container;

/** @var \GetRepo\Configurator\Configurator $configurator */
$configurator = (new Container())->getConfigurator();

$configurator(
    configuration: [/*yaml*/], // your configuration
    tags: ['files'], // optional filter tags
    env: 'prod', // optional env,
    input: $input, // optional input
    output: $output, // optional output
);
```


### Command-Line Interface (CLI)

```sh
Usage:
  vendor/get-repo/configurator/bin/configurator [options] [--] <path>

Arguments:
  path                  Path to yaml configuration file

Options:
  -t, --tags=TAGS       Tags to filter configurator (multiple values allowed)
  -e, --env=ENV         Sets environment for command process
  -h, --help            Display this help
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi|--no-ansi  Force (or disable --no-ansi) ANSI output
  -n, --no-interaction  Do not ask any interactive question
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```

<br/><br/>


## Configuration Reference

 - **tags** : Each actions can have a `tags` key to filter it in the service or CLI : `tags: ['custom_tag', 'another']`
 - **settings** : Defined settings to change the global action behavior:
   ```yaml
    settings:
        default_env: 'prod' # default is NULL
        env:
            env_file_path: '/path/to/.my.env' # default is '.env'
        composer:
            path: '/path/to/composer' # default is running 'which composer'
        yaml:
            inline: 4 # default is 2
            indent: 2 # default is 4
            flags_parse: !php/const Symfony\Component\Yaml\Yaml::PARSE_DATETIME # default is none (0)
            flags_dump: !php/const Symfony\Component\Yaml\Yaml::DUMP_OBJECT # default is none (0)
   ```

<br><br>

### Environment Variable Actions
Default tag is `env`
#### Create Env
```yaml
actions:
    -
        action: 'env'
        name: MYSQL_HOST
        value: localhost # values can have other env vars. e.g : 'my name is ${USER}'
        path: '.env.local' # (optional) override path default settings value
        check_if_exists: false  # (optional) default false. If env var already exists, get the existing value.
```

#### Ask Envs
```yaml
actions:
    -
        action: 'env_ask'
        names:
            MYSQL_HOST: localhost
            MYSQL_USER: root
        path: '.env.local' # (optional) override path default settings value
        check_if_exists: false  # (optional) default false. If env var value already exists, do not ask and get the existing value.
```

#### Remove Env
```yaml
actions:
    -
        action: 'remove_env'
        name: ENV_NAME
```

<br><br>

### File System Actions
Default tag is `filesystem`
#### Create Directory
```yaml
actions:
    -
        action: 'create_directory'
        path: path/to/my/directory
        mode: 0755 # optional
```

#### Create File
```yaml
actions:
    -
        action: 'create_file'
        path: path/to/my/file.txt
        content: |
            multiline
            content
```

#### Create YAML File
```yaml
actions:
    -
        action: 'create_yaml_file'
        path: path/to/my/file.yaml
        inline: 8 # (optional) overrides default settings value
        indent: 4 # (optional) overrides default settings value
        flags: !php/const Symfony\Component\Yaml\Yaml::DUMP_NULL_AS_TILDE # (optional) overrides default settings value
        content:
            key1: value1
            key2: [value2]
```

#### Copy Path (file and directory)
```yaml
actions:
    -
        action: 'copy_path'
        path: '%root%/path/from' # file or directory
        to: '%root%/path/to'
```

#### Move Path
```yaml
actions:
    -
        action: 'move_path'
        path: '%root%/path/from'
        to: '%root%/path/to'
```

#### Update File
```yaml
actions:
    -
        action: 'update_file'
        path: '/path/to/update'
        changes:
            -
                change: 'insert-line'
                line: 5
                content: 'content to add at line 5'
            -
                change: 'replace-first'
                search: 'find-me'
                content: 'replacement'
                exception_on_invalid_update: false # optional
            -
                change: 'replace-all'
                search: 'find-me'
                content: 'replacement'
                exception_on_invalid_update: false # optional
            -
                change: 'preg-replace'
                search: '/reg-(exp)/'
                content: '$1 replacement'
                exception_on_invalid_update: false # optional
```

#### Update YAML File
```yaml
actions:
    -
        action: 'update_yaml_file'
        property_path: '[property][path]'
        content:
            key1: value1
            key2: [value2]
    -
        action: 'update_yaml_file'
        property_path: '[property][]' # <--- note the open array notation
        content: 'new item'
    -
        action: 'update_yaml_file'
        property_path: '[property][path]'
        remove: true # remove [property][path] (key not incompatible with "content")  
```

#### Create Symlink
```yaml
actions:
    -
        action: 'create_symlink'
        path: '%root%/path/to/my/file'  # use %root% to avoid symlink creation error
        target: '%root%/path/to/target' # use %root% to avoid symlink creation error
```

#### Remove Path
```yaml
actions:
    -
        action: 'remove_path'
        path: '%root%/path/to/remove'
```

<br><br>

### Composer Actions
Default tag is `composer`
#### Composer Config
```yaml
actions:
    -
        action: 'composer_config'
        key: 'config_name'
        value: 'value'
```

#### Composer Require
```yaml
actions:
    -
        action: 'composer_require'
        options: ['--dev'] # lots of options available
        dependencies:
            - 'get-repo/configurator:^1.0'
            - 'get-repo/another-stuff:~3.2'
```

#### Composer Install
```yaml
actions:
    -
        action: 'composer_install'
        options: ['--no-dev', '--optimize-autoloader'] # lots of options available
```

#### Composer Update
```yaml
actions:
    -
        action: 'composer_update'
        options: ['--with-all-dependencies'] # lots of options available
```

#### Composer Run Script
```yaml
actions:
    -
        action: 'composer_run_script'
        name: 'script-name' # required
        options: ['--no-dev'] # run script option(s)
```

<br><br>

### Command Action
Default tag is `command`
#### Run Command
```yaml
actions:
    -
        action: 'run_command'
        command: "echo 'my custom command'"
```
